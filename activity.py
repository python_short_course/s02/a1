year = int(input("Please input a year:\n"))

if year % 4 == 0:
    print(f"{year} is a leap year")
elif year <= 0:
    print(f"{year} is not valid")
else:
    print(f"{year} is not a leap year")

row = int(input("Please enter number of rows:\n"))
col = int(input("Please enter number of columns:\n"))


for x in range(row):
    print('*' * col)

